package com.mafr.invitation.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class ResponseDTO<T> {

    private String timestamp = LocalDateTime.now().toString();
    private Integer status;
    private String message;
    @JsonIgnore
    private HttpStatus httpStatus;
    private T data;

    public ResponseDTO(HttpStatus status, T data) {
        this.data = data;
        this.status = status.value();
        this.message = status.getReasonPhrase();
        this.httpStatus = status;
    }
}
