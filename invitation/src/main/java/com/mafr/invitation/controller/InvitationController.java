package com.mafr.invitation.controller;

import com.mafr.invitation.dto.InvitationDTO;
import com.mafr.invitation.dto.ResponseDTO;
import com.mafr.invitation.entity.InvitationEntity;
import com.mafr.invitation.service.InvitationService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/invtation")
@Slf4j
@RequiredArgsConstructor
public class InvitationController {
    private final InvitationService service;

    @CrossOrigin
    @PostMapping
    public ResponseEntity<?> saveWish(@Valid @RequestBody InvitationEntity param) {
        ResponseDTO<InvitationEntity> response = new ResponseDTO<>(HttpStatus.OK, service.saveWish(param));
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @CrossOrigin
    @GetMapping("/{wedding}")
    public ResponseEntity<ResponseDTO<?>> getAll(@PathVariable String wedding) {
        try {
            ResponseDTO<List<InvitationDTO>> response = new ResponseDTO<>(HttpStatus.OK, service.getAll(wedding));
            return new ResponseEntity<>(response, response.getHttpStatus());
        } catch (Exception e) {
            ResponseDTO<?> response = new ResponseDTO<>(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage() + ", " + e.getCause());
            return new ResponseEntity<>(response, response.getHttpStatus());
        }
    }
}
