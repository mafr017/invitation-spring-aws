package com.mafr.invitation.controller;

import com.mafr.invitation.entity.InvitationEntity;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@RestController
@RequestMapping("/api/v1/test")
@Slf4j
public class TestController {
    @GetMapping
    public String sayHello() {
        String pattern = "HH:mm:ss, dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Long waktuSekarang = System.currentTimeMillis();
        System.out.println(waktuSekarang);
        String date = simpleDateFormat.format(new Date(waktuSekarang));
        System.out.println(date);
        return "Hello World! This message from API Backend.";
    }
}
