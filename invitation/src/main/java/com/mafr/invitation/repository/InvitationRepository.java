package com.mafr.invitation.repository;

import com.mafr.invitation.entity.InvitationEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvitationRepository extends JpaRepository<InvitationEntity, Long> {
    List<InvitationEntity> findAllByWedding(String wedding, Sort sort);
}
