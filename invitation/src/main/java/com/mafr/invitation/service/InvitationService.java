package com.mafr.invitation.service;

import com.mafr.invitation.dto.InvitationDTO;
import com.mafr.invitation.entity.InvitationEntity;

import java.util.List;

public interface InvitationService {

    InvitationEntity saveWish(InvitationEntity param);

    List<InvitationDTO> getAll(String wedding);
}
