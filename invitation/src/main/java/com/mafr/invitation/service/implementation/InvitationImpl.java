package com.mafr.invitation.service.implementation;

import com.mafr.invitation.dto.InvitationDTO;
import com.mafr.invitation.entity.InvitationEntity;
import com.mafr.invitation.repository.InvitationRepository;
import com.mafr.invitation.service.InvitationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Slf4j
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class InvitationImpl implements InvitationService {
    private final InvitationRepository repository;

    @Override
    @Transactional
    public InvitationEntity saveWish(InvitationEntity param) {
        Long waktuSekarang = System.currentTimeMillis();
        param.setCreatedDate2(waktuSekarang);
        LocalDateTime triggerTime =
                LocalDateTime.ofInstant(Instant.ofEpochMilli(waktuSekarang),
                        TimeZone.getDefault().toZoneId());

        log.info("WAKTU: {}", triggerTime);
        log.info("DATA: {}", param);
        repository.save(param);
        return param;
    }

    @Override
    public List<InvitationDTO> getAll(String wedding) {
        List<InvitationEntity> entityList = repository.findAllByWedding(wedding, Sort.by(Sort.Direction.DESC, "createdDate"));
        List<InvitationDTO> invitationDTO = new ArrayList<>();
        for (InvitationEntity el : entityList) {
            InvitationDTO data = new InvitationDTO();
            data.setId(el.getId());
            data.setWedding(el.getWedding());
            data.setName(el.getName());
            data.setWish(el.getWish());
            data.setWillCome(el.getWillCome());
            data.setPeoples(el.getPeoples());
            if (el.getCreatedDate2() != null) {
                String pattern = "HH:mm:ss, dd-MM-yyyy";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                String date = simpleDateFormat.format(new Date(el.getCreatedDate2()));
                data.setCreatedDate(date);
            } else {
                DateTimeFormatter format = DateTimeFormatter.ofPattern("HH:mm:ss, dd-MM-yyyy");
                data.setCreatedDate(el.getCreatedDate().format(format));
            }
            invitationDTO.add(data);
        }
        return invitationDTO;
    }
}
